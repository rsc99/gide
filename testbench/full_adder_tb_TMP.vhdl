library ieee;
use ieee.std_logic_1164.all;
use std.textio.all;
use ieee.std_logic_textio.all;

entity full_adder_tb is
end full_adder_tb;

architecture tb of full_adder_tb is

 component full_adder
 port(
 a : in std_logic;
 b : in std_logic;
 cin : in std_logic;
 s : out std_logic;
 cout : out std_logic
 );
 end component;
 
signal a : std_logic;
signal b : std_logic;
signal cin : std_logic;
signal s : std_logic;
signal cout : std_logic;



file input_buf : text;
begin
 uut: full_adder port map (
 a => a,
 b => b,
 cin => cin,
 s => s,
 cout => cout
 );

process
    constant period: time := 20 ns;
    variable read_col_from_input_buf : line;
    variable a_var : std_logic;
    variable b_var : std_logic;
    variable cin_var : std_logic;
    variable val_SPACE : character;
    begin
        file_open(input_buf,"/home/rsc/Desktop/repos_rsc/gide/testbench/full_adder_values.txt",  read_mode);
        readline(input_buf, read_col_from_input_buf); 
        while not endfile(input_buf) loop
            readline(input_buf, read_col_from_input_buf);
            read(read_col_from_input_buf, a_var);
            read(read_col_from_input_buf, val_SPACE);
            read(read_col_from_input_buf, b_var);
            read(read_col_from_input_buf, val_SPACE);
            read(read_col_from_input_buf, cin_var);
            a <= a_var;
            b <= b_var;
            cin <= cin_var;
            wait for period;
        end loop;
        file_close(input_buf);
        wait;
    end process;
end tb;

