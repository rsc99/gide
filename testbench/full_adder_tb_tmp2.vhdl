library ieee;
use ieee.std_logic_1164.all;
use std.textio.all;
use ieee.numeric_std.all;

entity full_adder_tb is
end full_adder_tb;

architecture tb of full_adder_tb is

component full_adder is port(
a : in std_logic;
b : in std_logic;
cin : in std_logic;
s : out std_logic;
q1 : in std_logic_vector(1 downto 0);
q2 : in std_logic_vector(1 downto 0);
q3 : out std_logic_vector(1 downto 0);
cout : out std_logic);
end component;

signal a : std_logic;
signal b : std_logic;
signal cin : std_logic;
signal s : std_logic;
signal q1 : std_logic_vector(1 downto 0);
signal q2 : std_logic_vector(1 downto 0);
signal q3 : std_logic_vector(1 downto 0);
signal cout : std_logic;

begin
uut: full_adder port map (
a => a,
b => b,
cin => cin,
s => s,
q1 => q1,
q2 => q2,
q3 => q3,
cout => cout
);
process
constant period: time := 30 ns;
begin
a <= '1'; b <= '1'; cin <= '0'; q1 <= "10"; q2 <= "01"; 
wait for period;

report "salida" & std_logic'image(s);
a <= '1'; b <= '1'; cin <= '1'; q1 <= "11"; q2 <= "11"; 
wait for period;

report "salida" & std_logic'image(s);
a <= '0'; b <= '0'; cin <= '0'; q1 <= "10"; q2 <= "10"; 
wait for period;

report "salida" & std_logic'image(s);
a <= '0'; b <= '0'; cin <= '0'; q1 <= "00"; q2 <= "11"; 
wait for period;

report "salida" & std_logic'image(s);

wait;
end process;
end;
