library ieee;
use ieee.std_logic_1164.all;
use std.textio.all;
use ieee.numeric_std.all;

entity full_adder_tb is
end full_adder_tb;

architecture tb of full_adder_tb is

component full_adder is port(
a : in std_logic;
b : in std_logic;
cin : in std_logic;
s : out std_logic;
cout : out std_logic);
end component;

signal a : std_logic;
signal b : std_logic;
signal cin : std_logic;
signal s : std_logic;
signal cout : std_logic;

begin
uut: full_adder port map (
a => a,
b => b,
cin => cin,
s => s,
cout => cout
);
process
constant period: time := 30 ns;
begin
report "60,ns";
report "a,cin,b,s,cout";

a <= '0'; cin <= '1'; b <= '1'; 
wait for period;

report std_logic'image(a) & "," & std_logic'image(cin) & "," & std_logic'image(b) & "," & std_logic'image(s) & "," & std_logic'image(cout);a <= '1'; cin <= '1'; b <= '1'; 
wait for period;

report std_logic'image(a) & "," & std_logic'image(cin) & "," & std_logic'image(b) & "," & std_logic'image(s) & "," & std_logic'image(cout);a <= '0'; cin <= '1'; b <= '0'; 
wait for period;

report std_logic'image(a) & "," & std_logic'image(cin) & "," & std_logic'image(b) & "," & std_logic'image(s) & "," & std_logic'image(cout);a <= '1'; cin <= '1'; b <= '1'; 
wait for period;

report std_logic'image(a) & "," & std_logic'image(cin) & "," & std_logic'image(b) & "," & std_logic'image(s) & "," & std_logic'image(cout);a <= '0'; cin <= '0'; b <= '0'; 
wait for period;

report std_logic'image(a) & "," & std_logic'image(cin) & "," & std_logic'image(b) & "," & std_logic'image(s) & "," & std_logic'image(cout);a <= '1'; cin <= '1'; b <= '1'; 
wait for period;

report std_logic'image(a) & "," & std_logic'image(cin) & "," & std_logic'image(b) & "," & std_logic'image(s) & "," & std_logic'image(cout);a <= '0'; cin <= '0'; b <= '0'; 
wait for period;

report std_logic'image(a) & "," & std_logic'image(cin) & "," & std_logic'image(b) & "," & std_logic'image(s) & "," & std_logic'image(cout);a <= '0'; cin <= '0'; b <= '0'; 
wait for period;

report std_logic'image(a) & "," & std_logic'image(cin) & "," & std_logic'image(b) & "," & std_logic'image(s) & "," & std_logic'image(cout);a <= '0'; cin <= '0'; b <= '0'; 
wait for period;

report std_logic'image(a) & "," & std_logic'image(cin) & "," & std_logic'image(b) & "," & std_logic'image(s) & "," & std_logic'image(cout);a <= '0'; cin <= '0'; b <= '0'; 
wait for period;

report std_logic'image(a) & "," & std_logic'image(cin) & "," & std_logic'image(b) & "," & std_logic'image(s) & "," & std_logic'image(cout);a <= '0'; cin <= '0'; b <= '0'; 
wait for period;

report std_logic'image(a) & "," & std_logic'image(cin) & "," & std_logic'image(b) & "," & std_logic'image(s) & "," & std_logic'image(cout);a <= '0'; cin <= '0'; b <= '0'; 
wait for period;

report std_logic'image(a) & "," & std_logic'image(cin) & "," & std_logic'image(b) & "," & std_logic'image(s) & "," & std_logic'image(cout);a <= '0'; cin <= '0'; b <= '0'; 
wait for period;

report std_logic'image(a) & "," & std_logic'image(cin) & "," & std_logic'image(b) & "," & std_logic'image(s) & "," & std_logic'image(cout);a <= '0'; cin <= '0'; b <= '0'; 
wait for period;

report std_logic'image(a) & "," & std_logic'image(cin) & "," & std_logic'image(b) & "," & std_logic'image(s) & "," & std_logic'image(cout);a <= '0'; cin <= '0'; b <= '0'; 
wait for period;

report std_logic'image(a) & "," & std_logic'image(cin) & "," & std_logic'image(b) & "," & std_logic'image(s) & "," & std_logic'image(cout);a <= '0'; cin <= '0'; b <= '0'; 
wait for period;

report std_logic'image(a) & "," & std_logic'image(cin) & "," & std_logic'image(b) & "," & std_logic'image(s) & "," & std_logic'image(cout);a <= '0'; cin <= '0'; b <= '0'; 
wait for period;

report std_logic'image(a) & "," & std_logic'image(cin) & "," & std_logic'image(b) & "," & std_logic'image(s) & "," & std_logic'image(cout);a <= '0'; cin <= '0'; b <= '0'; 
wait for period;

report std_logic'image(a) & "," & std_logic'image(cin) & "," & std_logic'image(b) & "," & std_logic'image(s) & "," & std_logic'image(cout);a <= '0'; cin <= '0'; b <= '0'; 
wait for period;

report std_logic'image(a) & "," & std_logic'image(cin) & "," & std_logic'image(b) & "," & std_logic'image(s) & "," & std_logic'image(cout);a <= '1'; cin <= '1'; b <= '1'; 
wait for period;

report std_logic'image(a) & "," & std_logic'image(cin) & "," & std_logic'image(b) & "," & std_logic'image(s) & "," & std_logic'image(cout);
wait;
end process;
end;