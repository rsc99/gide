from binascii import a2b_base64
from fileinput import filename
import sys
from PyQt5 import  QtWidgets
from gui import Ui_MainWindow
from PyQt5 import QtCore, QtGui, QtWidgets

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QPainter, QBrush, QPen, QColor, QPolygon
from PyQt5.QtCore import Qt, QPoint, pyqtSignal, pyqtSlot, QObject, QThread, QTimer
from PyQt5.QtWidgets import QMainWindow, QApplication, QPushButton, QWidget, QAction, QTabWidget,QVBoxLayout

from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.Qsci import *
import os
import sys
import shutil
from subprocess import PIPE, run, Popen
from qt_material import apply_stylesheet
import qdarkstyle
from tb_gen import *

tab_info = []
testbench_folder = "testbench"
color1 = "#00D30A" #std_logic, and-or
color2 = "#00D30A" #ieee, in-out, entity-architecture
color3 = "#c0c0c0" #caret
color4 = "#0fc0c0c0" #current_line
color5 = "#D2D2D2" #txt color
font = 'DejaVu Sans'

def load_file(filename):
    f = open(filename, "r")
    __editor = QsciScintilla()
    __editor.setText(f.read())
    __lexer = QsciLexerVHDL(__editor)

    __lexer.setPaper(QColor('#191919'))
    #__lexer.setColor(QColor('white'))
    __lexer.setDefaultPaper(QColor('#191919'))

    __lexer.setFont(QFont(font))
    __lexer.setColor(QColor(color5), 6)
    __lexer.setColor(QColor(color5), 5)
    __lexer.setColor(QColor(color5), 8)
    __lexer.setColor(QColor(color5), 9) 
    __lexer.setColor(QColor(color1), 12)
    __lexer.setColor(QColor(color1), 13)
    __lexer.setColor(QColor('yellow'), 3)

    myFont=QtGui.QFont(font)
    myFont.setBold(True)
    __lexer.setFont(myFont, 9)
    __lexer.setFont(myFont, 8)
    
    
    __editor.setLexer(__lexer)
    __editor.setUtf8(True)  # Set encoding to UTF-8
    __editor.setMarginsForegroundColor(QColor(100, 100, 100))
    __editor.setMarginBackgroundColor(1, QColor(0, 255 , 0))
    __editor.setMarginType(1, QsciScintilla.NumberMargin)
    __editor.setMarginWidth(1, 40)

    __editor.setIndentationsUseTabs(False)
    __editor.setTabWidth(4)
    __editor.setIndentationGuides(True)
    __editor.setTabIndents(True)
    __editor.setAutoIndent(True)
    

    __editor.setCaretForegroundColor(QColor(color3))
    __editor.setCaretLineVisible(True)
    __editor.setCaretLineBackgroundColor(QColor(color4))

    
    #__editor.setMarginLineNumbers(1, True)
    
    fullpath_filename = os.path.abspath(filename)
    tab_info.append([__editor, fullpath_filename])
    ui.tab_widget.addTab(__editor, os.path.basename(filename))
    tab_count = ui.tab_widget.count()
    ui.tab_widget.setCurrentIndex(tab_count)
    f.close()
    
def save_file(filename):
    f = open(filename, "w")
    current_tab = ui.tab_widget.currentIndex()
    txt = tab_info[current_tab][0].text()
    f.write(txt)
    ui.console_txt.setText(str(os.path.basename(filename)) + " was saved.")
    f.close()

def save_file_action_callback():
    index = ui.tab_widget.currentIndex()
    save_file(tab_info[index][1])

def load_file_action_callback():
    options = QFileDialog.Options()
    options |= QFileDialog.DontUseNativeDialog
    fileName, _ = QFileDialog.getOpenFileName(None,"QFileDialog.getOpenFileName()", "",";VHDL Files (*.vhdl)", options=options)
    if fileName:
        cols = list(zip(*tab_info))
        if(len(cols) > 0):
            if not(fileName in cols[1]):
                load_file(fileName)
        else:
            load_file(fileName)


def remove_tab_callback():
    index = ui.tab_widget.currentIndex()
    tab_info.pop(index)
    ui.tab_widget.removeTab(index)

def setColortoRow(table, rowIndex, color):
    for j in range(table.rowCount()):
        table.item(j, rowIndex).setBackground(color)

def run_callback():
    global testbench_folder
    save_file_action_callback()
    i = ui.tab_widget.currentIndex()
    base_filename = os.path.basename(tab_info[i][1])
    ui.console_txt.setText("Analyzing " + str(base_filename))
    if not(os.path.isdir(testbench_folder)):
        os.mkdir(testbench_folder) 
    shutil.copyfile(base_filename, testbench_folder+"/"+base_filename)

    command = ['ghdl', '-a', base_filename]
    result = run(command, stdout=PIPE, stderr=PIPE, universal_newlines=True, cwd=testbench_folder+"/")
    if(result.returncode == 0):
        ui.console_txt.append("Builded successfully")
        ui.console_txt.append(result.stdout)

        fill_table(base_filename)
    else:
        ui.console_txt.append("Error: " + result.stderr)
        ui.sim_groupbox.setVisible(False)

    
    





def sim_cancel_btn_callback():
    ui.sim_groupbox.setVisible(False)

def sim_set_time_btn_callback():
    rows = int(ui.sim_rows.text())
    period = ui.sim_period.text()
    period_scale = ui.sim_period_scale.currentText()

    ui.sim_table.setRowCount(rows)
    p = int(int(period) / 2)
    p_count = p
    row_period = []
    for i in range(0, rows):
        row_period.append(str(p_count)+" "+period_scale)
        p_count += p
    ui.sim_table.setRowCount(rows)
    ui.sim_table.setVerticalHeaderLabels(row_period)

    rows = ui.sim_table.rowCount()
    cols = ui.sim_table.columnCount()

    for j in range(0, rows):
        for i in range(0, cols):
            if not (ui.sim_table.item(j, i)):
                item = QTableWidgetItem("0")
                ui.sim_table.setItem(j,i, item)
                item.setTextAlignment(Qt.AlignCenter)

def run_sim_btn_callback():
    txt = get_table_values()
    entity_name = ui.sim_entity_txt.text()
    f = open(testbench_folder+"/"+entity_name+"_values.txt", "w")
    f.write(txt)
    f.close()

    testbench_txt = gen_testbench_txt(testbench_folder+"/"+entity_name+".vhdl")
    f = open(testbench_folder+"/"+entity_name+"_tb.vhdl", "w")
    f.write(testbench_txt)
    f.close()

    cmd_list = [1, 2, 3]
    for i in range(0, len(cmd_list)):
        res = exec_ghdl_cmd(cmd_list[i])
        if(res > 1):
            break;
    
    #print(get_table_header_names())
    
        
def get_table_header_names():
    names = []
    header = ui.sim_table.horizontalHeader()
    for col in range(ui.sim_table.columnCount()):
        names.append(ui.sim_table.horizontalHeaderItem(header.logicalIndex(col)).text())
    return names
def get_table_row(row):
    values = []
    header = ui.sim_table.horizontalHeader()
    for col in range(ui.sim_table.columnCount()):
        values.append(ui.sim_table.item(row, header.logicalIndex(col)).text())
    return values
def get_table_column(col):
    values = []
    header = ui.sim_table.verticalHeader()
    for row in range(ui.sim_table.rowCount()):
        values.append(ui.sim_table.item(header.logicalIndex(row), 0).text())
    return values
def get_table_values():
    rows = ui.sim_table.rowCount()
    cols = ui.sim_table.columnCount()
    period = ui.sim_period.text()
    period_scale = ui.sim_period_scale.currentText()
    table_values = period + "," + period_scale + "\n"
    h_headers = get_table_header_names()
    for i in range(0, len(h_headers)):
        table_values += h_headers[i]
        if(i != len(h_headers)-1):
            table_values += ","
    table_values += "\n"
    header = ui.sim_table.horizontalHeader()
    for j in range(0, rows):
        for i in range(0, cols):
            table_values += ui.sim_table.item(j, header.logicalIndex(i)).text()
            if(i != cols-1):
                table_values += ","
        if(j != rows-1):
            table_values += "\n"
    return table_values
def get_stop_time():
    period_str = ui.sim_period.text()
    period_scale_str = ui.sim_period_scale.currentText()
    rows_str = ui.sim_rows.text()
    period = int(period_str) * int(rows_str)
    return str(period) + period_scale_str
def exec_ghdl_cmd(index):
    entity_name = ui.sim_entity_txt.text()
    stop_time = get_stop_time()
    cmd = [['ghdl', '-a', entity_name+".vhdl"], 
            ['ghdl', '-a', '--ieee=synopsys', entity_name+"_tb.vhdl"],
            ['ghdl', '-e', '--ieee=synopsys', entity_name+"_tb"],
            ['ghdl', '-r', '--ieee=synopsys', entity_name+"_tb", "--stop-time="+stop_time, "--vcd=test.vcd"]
            ]
    result = run(cmd[index], stdout=PIPE, stderr=PIPE, universal_newlines=True, cwd=testbench_folder+"/")
    if(result.returncode == 0):
        if(len(result.stdout) > 0 and index == 3):
            lines = result.stdout.split("\n")
            lines = list(filter(None, lines))
            txt = ""
            for i in range(0, len(lines)):
                tmp = lines[i].split("(report note): ")[1]
                tmp = tmp.replace("\"", "")
                tmp = tmp.replace("\'", "")
                tmp = tmp.replace(" ", "")
                if(i != len(lines)-1):
                    txt += tmp +"\n"
                else:
                    txt += tmp
                #print(tmp)
            i = ui.tab_widget.currentIndex()
            base_filename = os.path.abspath(tab_info[i][1])
            entity_name = ui.sim_entity_txt.text()
            txt_filename = testbench_folder+"/"+entity_name+"_values.txt"
            fullpath = os.path.abspath(txt_filename)
            #print(txt)
            f = open(fullpath, "w")
            f.write(txt)
            f.close()
            #i = ui.tab_widget.currentIndex()
            #base_filename = os.path.basename(tab_info[i][1])
            #fill_table(base_filename)
            f = open(fullpath, "r")
            #print(f.read())
            f.close()
            print(base_filename)
            header = ui.sim_table.horizontalHeader()
    
            cmp, ports, entity_name = get_port_generic(base_filename)
            ui.sim_entity_txt.setText(entity_name)
            inputs, outputs, inoutputs = split_ports(ports)
            inputs_name = []
            for i in inputs:
                inputs_name.append(i[0])
            outputs_name = []
            for i in outputs:
                outputs_name.append(i[0])

            raw = get_txt_values(entity_name)
            txt_period = raw[0]
            txt_ports_name = raw[1].split(",")
            txt_values = raw[2:]
                
            cols = len(txt_ports_name)
            rows = len(txt_values)
            ui.sim_table.setRowCount(rows)
            ui.sim_table.setColumnCount(cols)
                
            for j in range(0, rows):
                line_values = txt_values[j].split(",")
                for i in range(0, cols):
                    item = QTableWidgetItem(line_values[i])
                    ui.sim_table.setItem(j,header.logicalIndex(i), item)
                    item.setTextAlignment(Qt.AlignCenter)
                    if(txt_ports_name[i] in outputs_name):
                        item.setFlags(QtCore.Qt.ItemIsEnabled)
                        item.setBackground(QColor("green"))
            #fill_table(base_filename)

    else:
        ui.console_txt.append("Error: " + result.stderr)
        #ui.sim_groupbox.setVisible(False)
        pass
    return result.returncode
def fill_table(base_filename):
    cmp, ports, entity_name = get_port_generic(base_filename)
    ui.sim_entity_txt.setText(entity_name)
    inputs, outputs, inoutputs = split_ports(ports)
    inputs_name = []
    for i in inputs:
        inputs_name.append(i[0])
    outputs_name = []
    for i in outputs:
        outputs_name.append(i[0])

    raw = get_txt_values(entity_name)
    txt_period = raw[0]
    txt_ports_name = raw[1].split(",")
    txt_values = raw[2:]
        
    cols = len(txt_ports_name)
    rows = len(txt_values)
    ui.sim_table.setRowCount(rows)
    ui.sim_table.setColumnCount(cols)
        
    for j in range(0, rows):
        line_values = txt_values[j].split(",")
        for i in range(0, cols):
            item = QTableWidgetItem(line_values[i])
            ui.sim_table.setItem(j,i, item)
            item.setTextAlignment(Qt.AlignCenter)
            if(txt_ports_name[i] in outputs_name):
                item.setFlags(QtCore.Qt.ItemIsEnabled)
                item.setBackground(QColor("green"))
                
    row_period = []
    p = int(int(txt_period.split(",")[0]) / 2)
    p_scale = txt_period.split(",")[1]
    p_count = p
    ui.sim_rows.setText(str(rows))
    ui.sim_period.setText(txt_period.split(",")[0])
    for i in range(0, rows):
        row_period.append(str(p_count)+" "+p_scale)
        p_count += p
    ui.sim_table.setHorizontalHeaderLabels(txt_ports_name)
    ui.sim_table.setVerticalHeaderLabels(row_period)
    ui.sim_groupbox.setVisible(True)
    idx = ui.sim_period_scale.findText(txt_period.split(",")[1])
    if idx != -1:
        ui.sim_period_scale.setCurrentIndex(idx)
    ui.sim_table.resizeColumnsToContents()
    ui.sim_table.resizeRowsToContents()

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)

    ui.sim_table.horizontalHeader().setSectionsMovable(True)
    ui.sim_table.verticalHeader().setSectionsMovable(True)

    if(len(sys.argv) > 1):
        load_file(sys.argv[1])

    #save_file("hola.txt")
    ui.actionSave.triggered.connect(save_file_action_callback)
    ui.actionOpen.triggered.connect(load_file_action_callback)
    ui.actionExit.triggered.connect(qApp.quit)
    ui.actionRun_simulation.triggered.connect(run_callback)
    ui.tab_widget.tabCloseRequested.connect(remove_tab_callback)

    #sim
    ui.sim_cancel_btn.clicked.connect(sim_cancel_btn_callback)
    ui.sim_set_time_btn.clicked.connect(sim_set_time_btn_callback)

    ui.run_sim_btn.clicked.connect(run_sim_btn_callback)

    ui.sim_groupbox.setVisible(False)
    
    #app.setStyleSheet(qdarkstyle.load_stylesheet(qt_api='pyqt5'))
    #apply_stylesheet(app, theme='dark_teal.xml')
    app.setStyle("Fusion")

    # Now use a palette to switch to dark colors:
    palette = QPalette()
    palette.setColor(QPalette.Window, QColor(53, 53, 53))
    palette.setColor(QPalette.WindowText, Qt.white)
    palette.setColor(QPalette.Base, QColor(25, 25, 25))
    palette.setColor(QPalette.AlternateBase, QColor(53, 53, 53))
    palette.setColor(QPalette.ToolTipBase, Qt.black)
    palette.setColor(QPalette.ToolTipText, Qt.white)
    palette.setColor(QPalette.Text, Qt.white)
    palette.setColor(QPalette.Button, QColor(53, 53, 53))
    palette.setColor(QPalette.ButtonText, Qt.white)
    palette.setColor(QPalette.BrightText, Qt.red)
    palette.setColor(QPalette.Link, QColor(42, 130, 218))
    palette.setColor(QPalette.Highlight, QColor(200, 200, 200))
    palette.setColor(QPalette.HighlightedText, Qt.black)
    app.setPalette(palette)

    ui.splitter_2.setSizes([4000, 1000, 1000])

    MainWindow.show()
    sys.exit(app.exec_())
