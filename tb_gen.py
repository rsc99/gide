import os
import sys
import shutil
import re

testbench_folder = "testbench"

def get_tb_name(filename):
    filename = str(os.path.basename(filename))
    shutil.copyfile(filename, testbench_folder+"/"+filename)
    tb_name = filename.lower()
    tb_name = filename.replace(".vhdl", "_tb")
    tb_name = filename.replace(".vhd", "_tb")
    return tb_name


def parse(a, txt):
    i = txt.lower().find(a) + len(a)
    bracket_count = 0
    last = 0
    for j in range(i, len(txt)):
        
        if(txt[j]==")"): 
            bracket_count -= 1
        if(txt[j]=="("): 
            bracket_count += 1
        if(bracket_count < 0):
            last = j
            break;
    return txt[i: last]
    
def extract_port(port):
    port_list = []
    lines = port.split(";")
    for line in lines:
        splt = line.split(":")
        ports = splt[0]
        ps = ports.split(",")
        assigns = splt[1]
        signal = ""
        if(assigns[0:3] == "out"):
            signal = "out"
        elif(assigns[0:5] == "inout"):
            signal = "inout"
        else:
            signal = "in"
        std_logic = assigns[len(signal):]
        bits = []
        if("(" in std_logic):
            in_brackets = parse("(", std_logic)
            if("downto" in in_brackets):
                splt2 = in_brackets.split("downto")
                bits = [splt2[0], splt2[1]]
                std_logic = std_logic.replace("downto", " downto ")
            elif("to" in in_brackets):
                splt2 = in_brackets.split("to")
                bits = [splt2[0], splt2[1]]
                std_logic = std_logic.replace("to", " to ")
        else:
            bits = ['1']
                
        
        for p in ps:
            port_list.append([p, signal, bits, std_logic])
    return port_list
        
            
def get_port_generic(filename):
    f = open(filename, "r")
    lines = f.readlines()
    txt = ""
    for line in lines:
        splt = line.split("--")
        if(len(splt) != 1):
            txt += splt[0]
        else:
            txt += line
    f.close()
    txt = txt.replace(" ","")
    txt = txt.replace("\n","")
    txt_original = txt
    txt = txt.lower()
    
    #txt is cleaner
    port = parse("port(", txt_original)
    generic = parse("generic(", txt)
    
    a = "entity"
    b = "isport("
    i = txt.lower().find(a) + len(a)
    j = txt.lower().find(b)
    entity_name = txt[i: j]
    
    cmp = ""
    cmp += "component " + entity_name + " is port(\n"
    ports = extract_port(port) #["name, in-out, bits(array), std_logic"]
    for i in range(0, len(ports)):
        port = ports[i]
        if(i == len(ports)-1):
            cmp += port[0] + " : " + port[1] + " " + port[3] + ");\nend component;"
        else:
            cmp += port[0] + " : " + port[1] + " " + port[3] + ";\n"
    return cmp, ports, entity_name
    
    
def get_port_map(entity_name, ports):
    txt = "uut: " + entity_name + " port map (\n"
    for i in range(0, len(ports)):
        port = ports[i]
        if(i == len(ports)-1):
            txt += port[0] + " => " + port[0] + "\n);"
        else:
            txt += port[0] + " => " + port[0] + ",\n"
    return txt
    

def split_ports(ports):
    inputs = []
    outputs = []
    inoutputs = []
    for i in range(0, len(ports)):
        if(ports[i][1]=="in"):
            inputs.append(ports[i])
    for j in range(0, len(ports)):
        if(ports[j][1]=="out"):
            outputs.append(ports[j])
    for k in range(0, len(ports)):
        if(ports[k][1]=="inout"):
            inoutputs.append(ports[k])
    return inputs, outputs, inoutputs


def gen_txt_values(entity_name, inputs):
    global testbench_folder
    
    if not(os.path.isdir(testbench_folder)):
        os.mkdir(testbench_folder) 

    txt_values_filename = testbench_folder+"/"+entity_name+"_values.txt"
    rows = 4
    txt_values = ""
    for i in range(0, rows):
        for j in range(0, len(inputs)):
            if(j == len(inputs)-1):
                txt_values += "0"
            else:
                txt_values += "0 "
        txt_values += "\n"

    if(os.path.exists(txt_values_filename)):
        f = open(txt_values_filename, "r")
        lines = f.readlines()
        line_test = lines[0]
        line_test = line_test.replace(" ", "")
        line_test = line_test.replace("\n", "")
        f.close()

        if(len(line_test) != len(inputs)):
            f = open(testbench_folder+"/"+entity_name+"_values.txt", "w")
            f.write(txt_values)
            f.close()
    else:
        f = open(testbench_folder+"/"+entity_name+"_values.txt", "w")
        f.write(txt_values)
        f.close()
    
    

def gen_testbench_txt(filename):
    filename = os.path.basename(filename)
    txt = ""
    txt += "library ieee;\nuse ieee.std_logic_1164.all;\nuse std.textio.all;\nuse ieee.numeric_std.all;\n\n"
    #we assuming that the file name = entity name
    cmp, ports, entity_name = get_port_generic(filename)

    inputs, outputs, inoutputs = split_ports(ports)
    #gen_txt_values(entity_name, inputs)

    txt_values_filename = testbench_folder+"/"+entity_name+"_values.txt"

    tb_name = entity_name + "_tb"
    txt += "entity " + tb_name + " is\nend " + tb_name + ";\n\narchitecture tb of " + tb_name + " is\n\n"
    
    txt += cmp
    txt += "\n\n"
    for i in ports:
        txt += "signal "+ i[0] + " : " + i[3] +";\n"
        
    #txt += "\nfile input_buf : text;\n\nbegin\n"
    txt += "\nbegin\n"
    txt += get_port_map(entity_name, ports)

    txt += "\nprocess"
    raw = get_txt_values(entity_name)
    tmp = raw[0].split(",")
    period = str(int(int(tmp[0])/2))
    period_scale = tmp[1]
    txt += "\nconstant period: time := " + period + " " +period_scale + ";"

    txt += "\nbegin\n"



    inputs, outputs, inoutputs = split_ports(ports)
    inputs_name = []
    for i in inputs:
        inputs_name.append(i[0])
    outputs_name = []
    for i in outputs:
        outputs_name.append(i[0])
    txt_ports_name = raw[1].split(",")
    txt_values = raw[2:]
    cols = len(txt_ports_name)
    rows = len(txt_values)

    txt += "report \"" + str(int(period) * 2) + "," + period_scale + "\";\n"
    txt += "report \"" + raw[1] + "\";\n\n"

    typelist = []
    a = ports
    b = txt_ports_name
    for j in range(0, len(b)):
        for i in range(0, len(b)):
            if(b[j]==a[i][0]):
                typelist.append(a[i][3])

    for j in range(0, rows):
        line_values = txt_values[j].split(",")
        for i in range(0, cols):
            if(txt_ports_name[i] in inputs_name):
                val = line_values[i]
                if(len(val)==1):
                    txt += txt_ports_name[i] + " <= \'" + val + "\'; "
                else: 
                    txt += txt_ports_name[i] + " <= \"" + val + "\"; "
        txt += "\nwait for period;\n"
        txt += "\nreport "
        for i in range(0, cols):
            txt += typelist[i]+"\'image("+txt_ports_name[i]+")"
            if(i != cols-1):
                txt += " & \",\" & "
            else:
                txt += ";"
            #txt += ports[j][3] + " "
        
        #txt += "report \"salida\" & std_logic\'image(s) & \",\"; \n"          
                
    '''for i in range(0, len(txt_values)):
        splt = txt_values[i].split(",")
        for j in range(0, len(splt)):
            val = splt[j]
            if(len(val)==1):
                txt += inputs[j][0] + " <= \'" + val + "\'; "
            else: 
                txt += inputs[j][0] + " <= \"" + val + "\"; "
        txt += "\nwait for period;\n"'''


        #txt += "\nreport \"salida\" & std_logic\'image(s);\n"
    txt += "\nwait;\nend process;\nend;"
    #add port_map
    return txt


def get_txt_values(entity_name):
    global testbench_folder
    f = open(testbench_folder+"/"+entity_name+"_values.txt", "r")
    lines = f.read()
    lines = lines.replace(" ","")
    splt = lines.split("\n") 
    splt = list(filter(None, splt))
    f.close()
    return splt

#print(gen_testbench_txt(sys.argv[1]))