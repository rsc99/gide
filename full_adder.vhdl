library ieee;
use ieee.std_logic_1164.all;
 
entity full_adder is
    port ( a : in std_logic;
    b : in std_logic;
    cin : in std_logic;
    s : out std_logic;
    q1, q2 : in std_logic_vector(2 downto 0);
    q3 : out std_logic_vector(2 downto 0);
    cout : out std_logic);
end full_adder;
 
architecture gate_level of full_adder is
begin
    s <= a xor b xor cin ;
    cout <= (a and b) or (cin and a) or (cin and b);
    q3 <= q1 and q2;
end gate_level;

